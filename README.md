# Ideal Papers

## Motivation

Ideal main goal is to find the cleanest way possible of making reusable application parts and compile them into more complex components such as React and Vue components

## Basic concepts

### Components

Constituting part of a larger whole. with `.component` file-extension and a specific data structure. [See the example here](./files/SimpleCounter.component)

## Components Structure

### Fundamental Structure

`App.component`:

```java
COMPONENT_NAME

IMPORTS {
    // Imports go here
}

TEMPLATE {
    // Markups go here
}

STYLE {
    // Styles go here
}

SCRIPT {
    // Scripts go here
}
```

As an example, this can be our `App.component`:

```java
MyApp

IMPORTS {
    Button: './ui/Button'
}

TEMPLATE {
    <div>
        <h2>{count}</h2>
        <Button onClick={increase}>
            One more time!
        </Button>
    </div>
}

STYLE {
    div {
        background: #222;
    }
}

SCRIPT {
    [count, setCount] = useState(0)
    const increase = () => {
        setCount(count + 1)
    }
}
```
